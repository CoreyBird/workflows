exports.default = function () {
    setTimeout(() => {
        console.log("Post-build step running!");
        //Move build artifacts to the www folder so they can be served as an update
    
        //List the files in the output directory
        var fs = require('fs');
        var files = fs.readdirSync('./dist/');
        
        //Search through the dist folder and find the highest versioned executable to move
        // This regex was tested against the patterns:
        // .cache                               (no match)
        // win-unpacked                         (no match)
        // builder-effective-config.yaml        (no match)
        // firstapp Setup 0.0.9.exe             (matched [0,0,9])
        // firstapp Setup 0.0.9.exe.blockmap    (no match)
        // firstapp Setup 0.0.10.exe            (matched [0,0,10])
        // firstapp Setup 0.0.10.exe.blockmap   (no match)
        // latest.yml                           (no match)
        let re = /.*?(\d+)\.(\d+)\.(\d+)\.exe$/;
        
        var best_major_ver = 0;
        var best_minor_ver = 0;
        var best_micro_ver = 0;
        var highest_version_filename = "";
        for( let filename of files ){
            var versions = filename.match(re);
            if( null == versions ){
                continue;
            }
            //versions[0] is the entire match string, the groups follow.
            // We want to find the highest versioned file, assuming it has a
            // Major.Minor.Micro style versioning system.
            var file_major_ver = parseInt(versions[1]);
            var file_minor_ver = parseInt(versions[2]);
            var file_micro_ver = parseInt(versions[3]);
            if( file_major_ver > best_major_ver ){
                best_major_ver = file_major_ver;
                best_minor_ver = file_minor_ver;
                best_micro_ver = file_micro_ver;
                highest_version_filename = versions[0];
            } else if( file_minor_ver > best_minor_ver ){
                best_minor_ver = file_minor_ver;
                best_micro_ver = file_micro_ver;
                highest_version_filename = versions[0];
            } else if( file_micro_ver > best_micro_ver ){
                best_micro_ver = file_micro_ver;
                highest_version_filename = versions[0];
            }
        }

        console.log("latest filename_found: " + highest_version_filename);
    
        //If we found a good filename, move the required files to the server folder for distribution
        if( "" != highest_version_filename ){

            //Copy the setup installer
            fs.copyFileSync("./dist/"+highest_version_filename, "./wwwroot/"+highest_version_filename);
                
            //Copy the blockmap
            fs.copyFileSync("./dist/"+highest_version_filename+".blockmap", "./wwwroot/"+highest_version_filename+".blockmap");

            //Finally, Copy the 'latest.yml' after everything else succeeds
            fs.copyFileSync("./dist/latest.yml", "./wwwroot/latest.yml");
        }
        
    }, 10000);

    //There seems to be a bug in the electron builder where it throws the 'afterAllArtifactBuild' event before
    // the latest.yml file is created. Putting in a delay here seems to be enough to let the builder create that
    // file and have it available for us.
    console.log("Post-build waiting 10 seconds before moving files around...");
}