import sys
import time
from datetime import timedelta

print("Waiting for process to start")
sys.stdout.flush()
time.sleep(3)

print("Process Started")
sys.stdout.flush()
start = time.time()
time.sleep(1)

try:
    while True:
        time.sleep(0.1)

        print("Run Time: {}".format(timedelta(seconds=int(time.time()-start))))
        sys.stdout.flush()
        
        if( (time.time()-start) > 10 ):
            break
        
        
except KeyboardInterrupt:
    pass

print("Done!")
sys.stdout.flush()
time.sleep(1)