const { app, BrowserWindow, ipcMain, Menu } = require('electron')
const fs = require('fs');
const path = require('path');

console.log("WorkFlows v" + app.getVersion());

function createWindow () {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 1536,
    height: 864,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
  win.loadFile('./index.html');

  const isDev = require('electron-is-dev');
  var update_check_busy = false;
  if( !isDev ){
    console.log("Working with release version, starting update checker...");
    const{ autoUpdater } = require('electron-updater')
    autoUpdater.on('error', (error) => {
      update_check_busy = false;
      console.log("An error occurred trying to download the update!");
      console.log(error);
    });
    autoUpdater.on('checking-for-update', () => {
      update_check_busy = true;
      console.log("Checking for updates!");
    });
    autoUpdater.on('update-available', () => {
      update_check_busy = true;
      console.log("Update available!");
    });
    autoUpdater.on('update-not-available', () => {
      update_check_busy = false;
      console.log("No update available!");
    });
    autoUpdater.on('download-progress', (progressObj) => {
      downloading_update = true;
      let log_message = "Download speed: " + progressObj.bytesPerSecond;
      log_message = log_message + ' - Downloaded ' + progressObj.percent + '%';
      log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
      console.log(log_message);
    });
    autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName, releaseDate, updateURL) => {
      update_check_busy = false;
      console.log("Update downloaded!");
      autoUpdater.quitAndInstall();
    });

    autoUpdater.checkForUpdates();
    setInterval(() => {
        if( !update_check_busy ){
          update_check_busy = true;
          autoUpdater.checkForUpdates();
        }
    }, 10000);
    
  } else {
    console.log("Working with development version, not starting update checker!");
    //win.webContents.openDevTools();
    win.webContents.send('version_update', "WorkFlows-Dev v" + app.getVersion());
  }

  win.webContents.on('did-finish-load', () => {
    if( isDev ){
      win.setTitle("WorkFlows-Dev v" + app.getVersion());
    } else{
      win.setTitle("WorkFlows v" + app.getVersion());
    }
    

  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
})

process_list = {};
ipcMain.on("launch_process",function (event, caller, id, args) {
    var return_call = caller + "|" + id;

    if( !(caller in process_list)){
      process_list[caller] = {}
    }

    var spawn = require("child_process").spawn;
    var process = spawn(args[0], args.slice(1));

    //Wait for output
    process.stdout.on('data', (data) => {
        // Do something with the data returned from python script
        event.sender.send("process_update|"+return_call, data.toString());
    });
    process.stderr.on('data', (data) => {
        event.sender.send("process_update|"+return_call, data.toString());
    });
    process.on('exit', (code, signal) => {
        event.sender.send("process_exit|"+return_call, code, signal);
    });

    process_list[caller][id] = process;
});
ipcMain.on("kill_process",function (event, caller, id, args) {
    if( caller in process_list && id in process_list[caller] ){
        process_list[caller][id].kill();
    };
});

var counter = 0;
function make_menu(){

  //ToDo: Populate the 'Window->New...' menu with the scripts in the "./windows" folder so
  // that modules can be dynamically added and removed for great flexibility. May not be
  // possible with Code Signing.
  const module_connector = require("./modules/connector.js");
  new_windows_submenu_list = []
  for( let module_title of module_connector.get_module_titles() ){
      new_windows_submenu_list.push({label: module_title, click: window_menu_handler});
  }

  //Populate the 'recently used' list. These are semi-auto saved by the user when they close
  // an in-use workflow without saving it. This way they can re-open edits they were working
  // on instead of having to re-populate a workflow from scratch again. These auto-saves will
  // be stored in the app's user space.
  var recent_workflows_list = [];
  try{
    var saves_dir = path.join(app.getPath('userData'), 'saves');
    var dir_listing = fs.readdirSync(saves_dir, {withFileTypes:true});
    //Gather all of the required information
    for( let dirent of dir_listing ){

      //Ignore the auto save
      if( "autosave_workspace.json" == dirent.name ){
        continue;
      }

      //ToDo: Delete really old user saves
      const stats = fs.statSync(path.join(app.getPath('userData'), 'saves', dirent.name));
      recent_workflows_list.push( {filepath:path.join(app.getPath('userData'), 'saves', dirent.name) ,filename:dirent.name, modified_time: stats.mtime} );
    }

    //Sort the files by time modified so that the newest ones are first in the list, then
    // only keep the top few.
    recent_workflows_list.sort((a,b)=> b.modified_time - a.modified_time);
    recent_workflows_list = recent_workflows_list.slice(0, 20);

    //now build the menu entries
    const current_time = Date.now();
    recent_files_menu_list = [];
    for( let entry of recent_workflows_list ){

      //The filenames are such that we have the format {guid.human_readable.json}, so we
      // can pick out a human readable name from the filename itself.
      var human_readable_name = entry.filename.substring(entry.filename.indexOf('.')+1, entry.filename.lastIndexOf('.'));

      //Generate a 'worked on xxx time ago' style adder to the human readible name. There can potentially be multiple of a
      // save with the same name, so specifying when the user worked on the workflow is helpful.
      var time_ago_string = "";
      var delta_time_seconds = (current_time - entry.modified_time) / 1000;
      if(delta_time_seconds < 86400 ){
          //Change was within last day, put the time on the end of the label
          time_ago_string = entry.modified_time.toTimeString().split(' ')[0];
      } else {
          time_ago_string = entry.modified_time.toLocaleDateString();
      }
    
    recent_files_menu_list.push(
      {
        label: human_readable_name + " (" + time_ago_string + ")",
        click: (menuItem, browserWindow, event) => {open_recent_workflow(browserWindow, entry.filepath.toString());}
      }
    );
  }
  } catch (e) {
    recent_files_menu_list = [];
  }

  const template = [
    {
      label: 'File',
      submenu: [
        { label: 'Recent', submenu:recent_files_menu_list },
        { label: 'New Workflow', click: file_menu_handler },
        { label: 'Save Workflow', click: file_menu_handler },
        { label: 'Open Workflow', click: file_menu_handler },
        //{ label: 'Close Workflow', click: file_menu_handler },
        { role: 'Quit' },
      ]
    },
    {
      label: 'Tabs',
      submenu: [
        {
          label: "New",
          submenu: new_windows_submenu_list
          // [
          //   For example:
          //   {label: "Code Editor", click: window_menu_handler},
          //   {label: "Query Manager", click: window_menu_handler},
          //   {label: "Job Manager", click: window_menu_handler},
          //   {label: "Processes", click: window_menu_handler},
          //   etc...
          // ]
        },
        {label: "Links", click: window_menu_handler},
      ]
    }];

    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);
    counter++;
}

make_menu();


function file_menu_handler(menuItem, browserWindow, event){
  browserWindow.send(menuItem['label']);
}
function window_menu_handler(menuItem, browserWindow, event){
  //Forward event to the renderer
  browserWindow.send(menuItem['label']);
}

function open_recent_workflow(browserWindow, filepath){
  //Send this event to the renderer
  if(filepath){
    browserWindow.send("Open Autosave", filepath);
  }
}

ipcMain.on("refresh_recents_menu",function () {
  //Remake the menu when workflows are closed so that we can re-populate the
  // 'Recent' submenu
  make_menu();
});