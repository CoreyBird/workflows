exports.get_module_title = function(){
    return "Code Editor";
}

exports.get_layout_options = function(){
    return {};
}

exports.get_default_config = function(){
    return {
        text:       null,
        filepath:   null,
        filename:   null,
        dirty:      false
    };
}

exports.html_object_creation = function(root_element, container, componentState){
    if( null == componentState.filepath ){
        var open_file_button = document.createElement("button");
        open_file_button.innerHTML = "Open File...";
        open_file_button.style = "margin: 20px";
        open_file_button.onclick = function(){
            dialog.showOpenDialog({ properties: ['openFile'] }).then((data) => {
                root_element.removeChild(open_file_button);
                create_file_viewer(data.filePaths[0], root_element, container, componentState);
            });
        };
        root_element.appendChild(open_file_button);
    } else {
        create_file_viewer(componentState.filepath, root_element, container, componentState);
    }
}

function create_file_viewer(filepath, root_element, container, componentState){
    const path = require('path');
    //Set up required external libraries and style sheets
    const CodeMirror = require("codemirror");

    //Load a bunch of common modes for syntax highlighting
    require("codemirror/mode/python/python");
    require("codemirror/mode/javascript/javascript");
    require("codemirror/mode/css/css");
    require("codemirror/mode/htmlmixed/htmlmixed");
    require("codemirror/mode/toml/toml");
    require("codemirror/mode/xml/xml");

    var stylesheet = document.createElement('link');
    stylesheet.setAttribute("rel","stylesheet");
    stylesheet.setAttribute("href", "file:///codemirror/lib/codemirror.css");
    document.getElementsByTagName("head")[0].appendChild(stylesheet);
    
    if( null == componentState.filepath || null == componentState.filename ){
        componentState.filepath = filepath;
        componentState.filename = path.basename(filepath);
        container.extendState({filepath:filepath, filename:path.basename(filepath)});
    }

    var text_area = document.createElement("textarea");
    text_area.style="height: 100%; width: 100%;";
    root_element.appendChild(text_area);

    //Try and detect the 'mode' from the file extension so we can
    // apply fun syntax highlighting
    var mode = null;
    switch( path.extname(filepath) ){
        case ".html":
            mode = "htmlmixed";
            break;
        case ".py":
            mode = "python";
            break;
        case ".css":
            mode = "css";
            break;
        case ".js":
            mode = "javascript";
            break;
        case ".toml":
            mode = "toml";
            break;
        case ".xml":
            mode = "xml";
            break;

    }

    //Create the code editor
    var myCodeMirror = CodeMirror.fromTextArea(text_area, {
    //var myCodeMirror = CodeMirror(text_area, {
            mode: mode,
            //autoRefresh: true,
            viewportMargin: Infinity,
            indentUnit: 4,
            lineNumbers: true, //This seems a little glitchy, disabling for now
            extraKeys: {
                "Ctrl-S": function(cm){save_function()}
            }
        });
    //Small delay for refresh. This makes sure everything on the page is loaded (ie libraries)
    // and that when 
    myCodeMirror.setSize("100%", "100%");
    setTimeout(function(){ myCodeMirror.refresh(); }, 100);

    //If the state isn't 'dirty', then load the file from disk. Otherwise
    // use whatever is saved in the component text state.
    if( componentState.dirty ){
        myCodeMirror.getDoc().setValue(componentState.text);
        container.setTitle(componentState.filename + ' *');
    } else {
        var contents = fs.readFileSync( componentState.filepath, 'utf-8');
        container.setTitle(componentState.filename);
        myCodeMirror.getDoc().setValue(contents);
    }

    myCodeMirror.on("keydown", function(cm, event){

        if( componentState.dirty ){
            // We're running this function to see if we are making the file dirty,
            // but if this file was already dirty then this would be a redundant check
            // so bail out early if that is the case.
            return;
        }

        //Don't need to capture these keys
        if( ("Control" == event.key)    ||
            ("Shift" == event.key)      ||
            ("ArrowUp" == event.key)    ||
            ("ArrowDown" == event.key)  ||
            ("ArrowLeft" == event.key)  ||
            ("ArrowRight" == event.key) ||
            ("End" == event.key)        ||
            ("Home" == event.key)       ||
            ("CapsLock" == event.key)   ||
            (event.ctrlKey && "s" == event.key) ||
            (event.ctrlKey && "r" == event.key) ){
            return;
        }

        // Make sure the title of the code editor has a star on it to indicate
        // that there are unsaved changes which indicates the file is dirty.
        container.setTitle(componentState.filename + " *");
    });
    myCodeMirror.on("keyup", function(cm, event){
        //ToDo: It would be better to not have to save this on every keystroke, but
        // there isn't a reliable way to know when this component is destroyed because
        // the destroy function doesn't seem to fire when the window is closed.
        container.extendState({text:myCodeMirror.getDoc().getValue()});
    });

    function save_function(){

        //Write the contents of the code editor to the file system
        fs.writeFile(componentState.filepath, myCodeMirror.getDoc().getValue(), function (err, file) {
            if( err ){
                console.log("Error saving file");
            }
            else{

                // If the save was successful, then the contents of this editor and the file system are
                // synchronized and the editor is no longer dirty.
                container.setTitle(componentState.filename);
                container.extendState({dirty:false});
            }
        });
    }
    
    container.on('resize',function() {
        myCodeMirror.setSize(container.width, container.height);
        myCodeMirror.refresh();
    });

    //ToDo: This function only seems to work sometimes, it doesn't seem to work if the
    // window is closed.
    // container.on('destroy',function() {
    //     // Save the state of the code editor on close if it was dirty. This allows
    //     // us to resume from where we left off when the program is re-opened.
    //     if( componentState.dirty ) {
    //         componentState.text = myCodeMirror.getDoc().getValue();
    //     }
    // });
}
