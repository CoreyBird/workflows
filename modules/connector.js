
var module_array = [
    require('./code_editor.js'),
    require('./query_manager.js'),
    require('./job_manager.js'),
    require('./process_manager.js')
];

module_dict = {};
for( let module of module_array ){
    module_title = module.get_module_title();
    module_dict[module_title] = module;
}

exports.get_module_titles = function(){
    var module_names_array = [];
    for( let module of module_array ){
        module_title = module.get_module_title();
        module_names_array.push(module_title);
    }
    return module_names_array;
}

exports.get_modules = function(){
    return module_array;
}

exports.get_module = function(module_title){
    return module_dict[module_title];
}