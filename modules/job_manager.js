
var callforward_func = null;
var jm_callback_function = null;
var jm_id_list = [];

exports.get_module_title = function(){
    return "Job Manager";
}

exports.get_default_config = function(){
    return {
        script_selection:null,
        query_results:[],
    };
}

exports.get_layout_options = function(){
    return {
        warn_before_close: true,
        is_scrollable: true,
    };
}

exports.html_object_creation = function(root_element, container, componentState){
    var query_result_div = document.createElement("div");
    query_result_div.className = "form-group";
    var query_result_label = document.createElement("label");
    query_result_label.innerHTML = "Cached query contained the following IDs:";
    query_result_label.htmlFor = "query_select";
    query_result_label.style = "margin-top: 10px; margin-left: 10px; color: white;";
    var query_text_area = document.createElement("textarea");
    query_text_area.id = "query_select";
    query_text_area.value = "";
    query_text_area.className = "form-control";
    query_text_area.rows = "3";
    query_result_div.appendChild(query_result_label);
    query_result_div.appendChild(query_text_area);

    //Load cached query if any exist
    if( null != componentState.query_results ){
        jm_id_list = componentState.query_results;
        for( let id of componentState.query_results ){
            query_text_area.value += id + '\n';
        }
    }

    root_element.appendChild(query_result_div);


    const callback_func = function(id_list){

        //Cache the query
        container.extendState({query_results:null}); //This seems unnecesary, but it is required so that the cache gets cleared!
        container.extendState({query_results:id_list});
        jm_id_list = id_list;

        //Clear out the query_result
        query_text_area.value = "";

        //Repopulate with the new entries
        for( let id of id_list ){
            query_text_area.value += id + '\n';
        }
    }
    jm_callback_function = callback_func;

    var script_select = document.createElement("select");
    script_select.className = "form-control";
    script_select.id = "script_select";

    process_scripts = [{script_path:'stdout_script.py', script_name:"STDOUT Only Script"}, {script_path:'print_to_file_script.py', script_name:"Print To File Script"}];
    for( let option of process_scripts ){
        var script_option = document.createElement("option");
        script_option.value = option.script_path;
        script_option.innerHTML = option.script_name;
        script_select.appendChild(script_option);
    }
    if( null != componentState.script_selection ){
        script_select.value = componentState.script_selection;
    }

    var select_div = document.createElement("div");
    select_div.className = "form-group";
    var select_label = document.createElement("label");
    select_label.innerHTML = "Select a script to run:";
    select_label.htmlFor = "script_select";
    select_label.style = "margin-top: 10px; margin-left: 10px; color: white;";
    select_div.appendChild(select_label);
    select_div.appendChild(script_select);

    root_element.appendChild(select_div);

    var launch_button = document.createElement("button");
    launch_button.innerHTML = "Launch Process";
    launch_button.className = "btn btn-xs btn-success";
    launch_button.style = "margin: 10px;"
    
    launch_button.onclick = function(){
        var script_option = script_select.value;
        container.extendState({script_selection:script_option});

        //Find a new output folder for this run
        var output_folder = null;
        var process_args = null;
        if( "print_to_file_script.py" == script_option ){
            output_folder = "T:\\Electronics\\Electron\\FirstApp\\py-script_output";
            process_args = ['python', script_option, output_folder ].concat(jm_id_list);
        } else {
            process_args = ['python', script_option ].concat(jm_id_list);
        }

        params = {args:process_args, output_folder:output_folder};
        callforward_func(params);
    }

    root_element.appendChild(launch_button);
}

exports.output_register_function = function(call_forward_function){
    callforward_func = call_forward_function;
}

exports.input_callback = function(args){
    jm_callback_function(args);
}

    