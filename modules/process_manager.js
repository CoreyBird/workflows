

var pm_callback_function = null;
var process_counter = 0;

exports.get_module_title = function(){
    return "Processes";
}

exports.get_default_config = function(){
    return {
        process_list:[]
    };
}

exports.get_layout_options = function(){
    return {
        warn_before_close: true,
        is_scrollable: true,
    };
}

exports.html_object_creation = function(root_element, container, componentState){
    //Create a 'list' to keep the processes organized
    var list_div = document.createElement("div");
    list_div.id = "ProcessList";
    list_div.style = "overflow: auto;";
    root_element.appendChild(list_div);

    function callback_func(params){
        process_counter++;

        var args = params.args;
        var process_uuid = generateUUID();

        var process_div = document.createElement("div");
        process_div.style = "float: left; width: 100%; margin-bottom: 3px;";
        process_div.style.backgroundColor = "#dddddd";
        var process_title = document.createElement("h2");
        process_title.innerHTML = "Process " + process_counter;
        var kill_button = document.createElement("button");
        kill_button.innerHTML = "Kill Process";
        kill_button.className = "btn btn-xs btn-warning"
        kill_button.onclick = function(){
            ipcRenderer.send("kill_process", "process_manager", process_uuid);
        }
        var process_text = document.createElement("p");
        process_text.innerHTML = "Command: \"" + args.join(' ') + "\"";
        var stdout_text = document.createElement("textarea");
        stdout_text.value = "";
        stdout_text.style = "margin: 5px; width: calc(100% - 20px);";
        stdout_text.className = "console_class";
        stdout_text.rows = "3";
        stdout_text.setAttribute("readonly", true);
        var termination_text = document.createElement("p");
        termination_text.innerHTML = "";

        process_div.appendChild(process_title);
        process_div.appendChild(kill_button);
        process_div.appendChild(process_text);
        process_div.appendChild(stdout_text);
        process_div.appendChild(termination_text);
        
        if( params.output_folder != null){
            var open_folder_button = document.createElement("p");
            open_folder_button.innerHTML = "Open Folder";
            open_folder_button.className = "btn btn-xs btn-success";
            open_folder_button.style = "margin: 10px;"
            open_folder_button.onclick = function(){
                const { shell } = require('electron');
                console.log("Opening:");
                console.log(params.output_folder);
                shell.openItem(params.output_folder);
            }
            process_div.appendChild(open_folder_button);
        }

        if( list_div.childElementCount > 0 ){
            list_div.insertBefore(process_div, list_div.childNodes[0]);
        } else {
            list_div.appendChild(process_div);
        }
        
        ipcRenderer.send("launch_process", "process_manager", process_uuid, args);

        ipcRenderer.on('process_update|process_manager|' + process_uuid, function(event, string){
            stdout_text.value += string;
            stdout_text.scrollTop = stdout_text.scrollHeight;
        });
        ipcRenderer.on('process_exit|process_manager|' + process_uuid, function(event, code, signal){
            process_div.removeChild(kill_button);
            if( null == signal){
                termination_text.innerHTML = "Process has exited with code: " + code;
            } else {
                termination_text.innerHTML = "Process has exited because of signal: " + signal;
            }
            
            var clean_up_button = document.createElement("button");
            clean_up_button.innerHTML = "Remove";
            clean_up_button.className = "btn btn-xs btn-danger pull-right"
            clean_up_button.style = "margin: 10px;"
            clean_up_button.onclick = function(){
                list_div.removeChild(process_div);
            }
            process_div.appendChild(clean_up_button);
        });
    }

    pm_callback_function = callback_func;

}

exports.input_callback = function(args){
    pm_callback_function(args);
}

