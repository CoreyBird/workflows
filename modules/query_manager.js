
var callforward_func = null;

exports.get_module_title = function(){
    return "Query Manager";
}

exports.get_layout_options = function(){
    return {
        warn_before_close: true,
        is_scrollable: true,
    };
}

exports.get_default_config = function(){
    return {
        saved_query:null,
    };
}

exports.html_object_creation = function(root_element, container, componentState){
    var stylesheet = document.createElement('link');
    stylesheet.setAttribute("rel","stylesheet");
    stylesheet.setAttribute("href", "file:///node_modules/jQuery-QueryBuilder/dist/css/query-builder.default.min.css");
    document.getElementsByTagName("head")[0].appendChild(stylesheet);

    var stylesheet = document.createElement('link');
    stylesheet.setAttribute("rel","stylesheet");
    stylesheet.setAttribute("href", "https://querybuilder.js.org/node_modules/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css");
    document.getElementsByTagName("head")[0].appendChild(stylesheet);

    require("jQuery-QueryBuilder");

    var query_button = document.createElement('button');
    query_button.innerHTML = "Run Query";
    query_button.className = "btn btn-xs btn-success";
    query_button.style = "margin: 10px;"

    var load_button = document.createElement('button');
    load_button.innerHTML = "Load Query";
    load_button.className = "btn btn-xs btn-success";
    load_button.style = "margin: 10px;"

    var save_button = document.createElement('button');
    save_button.innerHTML = "Save Query";
    save_button.className = "btn btn-xs btn-success";
    save_button.style = "margin: 10px;"

    var clear_button = document.createElement('button');
    clear_button.innerHTML = "Clear Query";
    clear_button.className = "btn btn-xs btn-success pull-right";
    clear_button.style = "margin: 10px;"

    var buttons_div = document.createElement('div');
    buttons_div.appendChild(query_button);
    buttons_div.appendChild(load_button);
    buttons_div.appendChild(save_button);
    buttons_div.appendChild(clear_button);
    root_element.appendChild(buttons_div);

    var query_div = document.createElement('div');
    root_element.appendChild(query_div);

    //ToDo: Farm out the database interaction to a python script. It should give a string from stdout
    // that is the JSON list that we need for the filter parameter.
    var spawnSync = require("child_process").spawnSync;
    var process = spawnSync('python', ['query_db.py', "get_filters"]);//spawnSync('python', ['get_db_filters.py']);
    var filters_json = {filters:[]};
    if(process.status){
        console.log('error!');
        console.log(process.stderr.toString());
        componentState.saved_query = null;
        return;
    } else {
        var raw_json = process.stdout.toString();
        filters_json = JSON.parse(raw_json);
    }

    $(query_div).queryBuilder({
        plugins: [
            'filter-description',
            'unique-filter',
            'bt-tooltip-errors',
            'bt-checkbox',
        ],
        filters: filters_json.filters,
        rules: componentState.saved_query
     });

     clear_button.onclick = function(){
        $(query_div).queryBuilder('reset');
     }

     query_button.onclick = function(){
        console.log("run query");
        console.log(query_div);
        var json_result = $(query_div).queryBuilder('getRules');
        console.log("get json");
        //var sql_result = $(query_div).queryBuilder('getSQL', 'question_mark');
        var sql_result = $(query_div).queryBuilder('getSQL', false);
        console.log("get sql");
        var spawnSync = require("child_process").spawnSync;
        var process = spawnSync('python', ['query_db.py', "get_ids", sql_result.sql]);
        var ids_list = {ids:[]};
        if(process.status){
            console.log('error!');
            console.log(process.stderr.toString());
            return;
        } else {
            var raw_ids_list = process.stdout.toString();
            ids_list = JSON.parse(raw_ids_list);
        }
        callforward_func(ids_list.ids);

        //Save query so we can load it again when we load this component again
        container.extendState({saved_query:json_result});
     }
     
}

exports.output_register_function = function(call_forward_function){
    callforward_func = call_forward_function;
}
