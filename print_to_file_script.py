import pandas
import sqlite3
import sys
import os

print("Creating CSV output")

output_folder_path = sys.argv[1]
if( not os.path.exists(output_folder_path) ):
    os.mkdir(output_folder_path)

ids_list = [sys.argv[i+2] for i in range(len(sys.argv)-2)]


#Get the details for each of the ID's and print them to a csv
conn = sqlite3.connect("testing.db")
df = pandas.read_sql_query("""
SELECT tests.rowid, user_name, ex_option, test_description 
FROM tests JOIN (SELECT rowid, * FROM users) AS x ON x.rowid = user_id
WHERE tests.rowid IN (%s)""" % ','.join(ids_list), conn)

df.to_csv(os.path.join(output_folder_path, 'query_dump.csv'), index=False)
print("CSV output created successfully")