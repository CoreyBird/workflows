import sys
import json
import sqlite3

if( 'get_filters' == sys.argv[1]):
    conn = sqlite3.connect('testing.db')
    cur = conn.cursor()

    filters = []

    #test ID filter
    filter = {'id':'rowid', 'label':'Test ID', 'type':'integer'}
    filters.append(filter)

    #Get the list of users
    filter = {'id':'user_id', 'label':'User', 'type':'integer', 'input':"checkbox", 'multiple':True, 'color':'primary', 'values':{}, 'operators':['in'] }
    cur.execute("SELECT rowid, user_name FROM users")
    data = cur.fetchall()
    for row in data:
        filter['values'][row[0]] = row[1]
    filters.append(filter)

    #Get the list of options
    filter = {'id':'ex_option', 'label':'Ex Option', 'type':'string', 'input':"radio", 'color':'primary', 'values':[], 'operators':['equal'] }
    cur.execute("SELECT DISTINCT(ex_option) FROM tests")
    data = cur.fetchall()
    for row in data:
        filter['values'].append(row[0])
    filters.append(filter)

    output_json = {'filters':filters}
    print(json.dumps(output_json))
if( 'get_ids' == sys.argv[1] ):
    query_string = sys.argv[2]
    
    conn = sqlite3.connect('testing.db')
    cur = conn.cursor()
    
    cur.execute("SELECT rowid FROM tests WHERE " + query_string)
    data = cur.fetchall()
    
    id_list = []
    if( data is not None and len(data) > 0 ):
        id_list = [row[0] for row in data]
    
    output_json = {'ids':id_list}
    print(json.dumps(output_json))
    
    