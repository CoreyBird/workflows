import pandas
import sqlite3
import sys
import os

print("Creating output...")

ids_list = [sys.argv[i+1] for i in range(len(sys.argv)-1)]


#Get the details for each of the ID's and print them to a csv
conn = sqlite3.connect("testing.db")
df = pandas.read_sql_query("""
SELECT tests.rowid, user_name, ex_option, test_description 
FROM tests JOIN (SELECT rowid, * FROM users) AS x ON x.rowid = user_id
WHERE tests.rowid IN (%s)""" % ','.join(ids_list), conn)

print(df.to_string(index=False))

print("Finished!")